use std::io::prelude::*;
use std::fs::File;
use std::io::{self, BufReader, SeekFrom};
use std::env;
use std::str::FromStr;
use std::string::String;
use regex::bytes::Regex;
use regex::bytes::Matches;
use std::cmp;
use clap::{App, Arg, SubCommand};

#[derive(Debug, PartialEq)]
enum PartitionFormat {
    Zero,
    One,
    Smaller128,
    FullRange,
    TwoFiveFive,
}

struct Partiton {
    start: u64,
    end: u64,
    format: PartitionFormat,
}

impl FromStr for PartitionFormat {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Zero" => Ok(PartitionFormat::Zero),
            "One" => Ok(PartitionFormat::One),
            "Smaller128" => Ok(PartitionFormat::Smaller128),
            "FullRange" => Ok(PartitionFormat::FullRange),
            "TwoFiveFive" => Ok(PartitionFormat::TwoFiveFive),
            _ => Err(()),
        }
    }
}

#[derive(Debug, PartialEq)]
struct Finding {
    start: u64,
    end: u64,
}

fn buildPartitions<R: Read>(stream: &mut R, partition_size: usize) -> Vec<Partiton> {
    let mut result = Vec::new();
    let mut buffer = vec![0u8; partition_size];
    let mut cnt: u64 = 0;
    let mut start: u64 ;
    let mut end: u64;
    loop {
        match stream.read(&mut *buffer) {
            Ok(n) => {
                if n == 0 {
                    break;
                }
                let mut min: u8 = 255;
                let mut max: u8 = 0;
                for b in buffer.iter() {
                    if *b < min {
                        min = *b;
                    }
                    if *b > max {
                        max = *b;
                    }
                }
                let format = if min == max && min == 0 {
                    PartitionFormat::Zero
                } else if min == max && min == 1 {
                    PartitionFormat::One
                } else if min == max && min == 255 {
                    PartitionFormat::TwoFiveFive
                } else if max < 128 {
                    PartitionFormat::Smaller128
                } else {
                    PartitionFormat::FullRange
                };
                start = cnt;
                end = start + (n - 1) as u64;
                result.push(Partiton{start, end, format});
                cnt += n as u64;
            }
            Err(_e) => {
                break;
            }
        };
    }

    return result;
}

fn loadPartitions<R: Read>(stream: &mut R) -> (Vec<Partiton>, String) {
    let mut result = Vec::new();
    let stream = BufReader::new(stream);
    let mut filename = "".to_string();

    for line in stream.lines() {
        let line = line.unwrap();
        let words = line.split(' ');
        let word: Vec<&str> = words.collect();
        if word[0] == "start:" {
            let start = word[1].parse::<u64>().unwrap();
            let end = word[3].parse::<u64>().unwrap();
            let format = word[5].parse::<PartitionFormat>().unwrap();
            result.push(Partiton{start, end, format});
        }
        if word[0] == "file:" {
            filename = word[1].clone().to_string();
        }
    }

    return (result, filename);
}

fn loadFindings<R: Read>(stream: &mut R) -> (Vec<Finding>, String) {
    let mut result = Vec::new();
    let stream = BufReader::new(stream);
    let mut filename = "".to_string();

    for line in stream.lines() {
        let line = line.unwrap();
        let words = line.split(' ');
        let word: Vec<&str> = words.collect();
        if word[0] == "finding:" {
            let start = word[2].parse::<u64>().unwrap();
            let end = word[4].parse::<u64>().unwrap();
            result.push(Finding{start, end});
        }
        if word[0] == "file:" {
            filename = word[1].clone().to_string();
        }
    }

    return (result, filename);
}

/*
We chop our stream into multiple checks and run regex on each pair, we need to overlap a bit, so that we find findings over the edges...
*/
/*
 [0..99]
 [300..499]
 [500..550]

 [0..99][300..399][400..499][500..550]
*/
fn interateIndex<R: Read + Seek>(stream: &mut R, index: Vec<Partiton>, f: &Fn(&[u8]) -> Vec<Finding>) -> Vec<Finding> {
    const READSIZE: usize  = 1  * 1024 * 1024;
    const BUFFERSIZE: usize  = 100 * READSIZE;
    let mut result: Vec<Finding> = vec![];
    let mut buffer = vec![0u8; BUFFERSIZE];
    let mut target_next_start = 0usize; //in buffer
    let mut source_next_start = 0i64; // from file
    for entry in index.iter() {
        if entry.format != PartitionFormat::Zero {
            if source_next_start < entry.start as i64 {
                source_next_start = entry.start as i64;
                stream.seek(SeekFrom::Start(source_next_start as u64));
            }
            while entry.end as i64 > source_next_start+1 {
                let seekdiff = entry.start as i64 - source_next_start;
                if seekdiff != 0 {
                    stream.seek(SeekFrom::Start(source_next_start as u64));
                }
                match stream.read(&mut buffer[target_next_start..target_next_start+READSIZE]) {
                    Ok(n) => {
                        if n == 0 {
                            break;
                        }
                        let tmp = f(&buffer[target_next_start..target_next_start+n]);
                        for f in tmp {
                            result.push(Finding{ start: f.start + source_next_start as u64, end: f.end + source_next_start as u64 } );
                        }

                        let oldstart = target_next_start;
                        target_next_start += n as usize;
                        source_next_start += n as i64;
                        if target_next_start + READSIZE > BUFFERSIZE {
                            //copy down last READSIZE
                            let i = 0;
                            for i in 0..READSIZE {
                                buffer[i] = buffer[oldstart+i];
                            }
                            target_next_start = i+1;
                        }
                    }
                    Err(_e) => {
                        break;
                    }
                };
            }
        }
    }
    result
}

fn displayFindings<R: Read + Seek>(stream: &mut R, findings: Vec<Finding>, context: u64) {
    const BUFFERSIZE: usize  = 10 * 1024 * 1024;
    let mut buffer = vec![0u8; BUFFERSIZE];
    for finding in findings.iter() {
        let start = (finding.start - context).max(0);
        let end = (finding.end + context).min(/*TODO GET TOTAL SIZE*/ 1024*1024*1024*1024*1024*1024);
        let len = (end - start) as usize;
        stream.seek(SeekFrom::Start(start));
        match stream.read(&mut buffer[0..len]) {
            Ok(n) => {
                println!("{}", displayString(&buffer[0..n]));
            }
            Err(_e) => {
                break;
            }
        }
    }
}

fn displayString(str: &[u8]) -> String {
    let mut result = "".to_string();
    for s in str.iter() {
        result.push(*s as char);
    }
    result
}


fn main() {
    let matches = App::new("brep")
        .version("0.1")
        .about("binary search a regular expression and print")
        .author("Marcel Märtens")
        .subcommand(SubCommand::with_name("index")
            .arg(Arg::with_name("file")
                .help("file to create index of")
                .index(1)
                .required(true)
                .takes_value(true)))
        .subcommand(SubCommand::with_name("find")
            .arg(Arg::with_name("file")
                .help("file to create index of")
                .index(1)
                .required(true)
                .takes_value(true))
            .arg(Arg::with_name("regex")
                .help("regular expression to search")
                .short("r")
                .multiple(true)
                .required(true)
                .takes_value(true))
            .arg(Arg::with_name("skip")
                .help("areas to skip according to index")
                .short("s")
                .required(false)
                .takes_value(true)))
        .subcommand(SubCommand::with_name("display")
            .arg(Arg::with_name("file")
                .help("findings to display")
                .index(1)
                .required(true)
                .takes_value(true))
            .arg(Arg::with_name("context")
                .help("context around findings")
                .short("c")
                .required(false)
                .takes_value(true)))
        .get_matches();
    if let Some(matches) = matches.subcommand_matches("index") {
        let file_name = matches.value_of("file").unwrap();
        const BUFFERSIZE: usize  = 10 * 1024 * 1024;
        let mut f = File::open(file_name).unwrap();

        let partitions = buildPartitions(&mut f, BUFFERSIZE);
        println!("brep 1");
        println!("file: {}", file_name);
        for p in &partitions {
            println!("start: {} end: {} enum: {:?}", p.start, p.end, p.format);
        }
        println!("#statistic");
        let mut stat = [0; 5];
        let mut total: f32 = partitions.len() as f32;
        for p in &partitions {
            match &p.format {
                PartitionFormat::Zero => stat[0] += 1,
                PartitionFormat::One => stat[1] += 1,
                PartitionFormat::Smaller128 => stat[2] += 1,
                PartitionFormat::FullRange => stat[3] += 1,
                PartitionFormat::TwoFiveFive => stat[4] += 1,
            }
        }
        println!("#0: {}, 1: {}, <128: {}, 0-255: {} 255: {}", stat[0], stat[1], stat[2], stat[3], stat[4]);
        println!("#0: {}, 1: {}, <128: {}, 0-255: {} 255: {}", stat[0] as f32 / total, stat[1] as f32 / total, stat[2] as f32 / total, stat[3] as f32 / total, stat[4] as f32 / total);

    } else if let Some(matches) = matches.subcommand_matches("find") {
        let filters: Vec<&str> = matches.values_of("regex").unwrap().collect();
        let file_name = matches.value_of("file").unwrap();
        let mut f = File::open(file_name).unwrap();
        let (partitions, indexed_file) = loadPartitions(&mut f);
        println!("#searching in file: {}", indexed_file);
        let mut f = File::open(&indexed_file).unwrap();

        let mut regexes: Vec<Regex> = vec![];
        for f in filters {
            regexes.push(Regex::new(f).unwrap());
            println!("#search for: {}", f);
        }
        let findings = interateIndex(&mut f, partitions, &|buffer| {
            let mut result = vec![];
            for re in &regexes {
                let matches = re.find_iter(&buffer);
                for m in matches {
                    result.push(Finding{start: m.start() as u64, end: m.end() as u64});
                }
            }
            result
        });
        println!("#found: {}", findings.len());

        println!("file: {}", indexed_file);
        for finding in findings {
            println!("finding: start: {} end: {}",  finding.start, finding.end);
        }

    } else if let Some(matches) = matches.subcommand_matches("display") {
        //let mut index;
        let file_name = matches.value_of("file").unwrap();
        let mut f = File::open(file_name).unwrap();
        let (findings, findings_file) = loadFindings(&mut f);
        println!("#displaying file: {}", findings_file);
        let context: u64 = match matches.value_of("context") {
            Some(n) => n.parse().unwrap(),
            None => 0,
        };
        let mut f = File::open(&findings_file).unwrap();

        displayFindings(&mut f, findings, context);

    }
}
