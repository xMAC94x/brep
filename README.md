# BREP - binary search a regular expression and print

binary grep like utility
- espacially usefull to search for strings and surrounding in binary files like raw /dev/sdx drives

## Index files
```
sudo brep index /dev/sda >> sda.index
```

## Find for certain regexes
```
sudo brep find sda.index -r "someregex" >> sda.findings
```

## Display results with context
```
sudo brep display sda.findings -c 50
```

# How to compile
```
#install rust toolchain first: https://www.rust-lang.org/tools/install
cargo build --release
./target/release/brep --help
```